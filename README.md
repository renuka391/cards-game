# README #

### Dependencies guidelines ###

* API project setup is necessary

MYSQL Database setup =>
* Run LAMP/MAMP and import schema.sql
* It's inside cardsappapi.zip
* run the schema.sql in phpmyadmin to create the database.

* LAMP/MAMP setup =>
* extract the contents of cardsappapi.zip to htdocs folder as htdocs/cardsappapi
* change database username/password in htdocs/cardsappapi/index.php line no. 11
    "R::setup('mysql:host=localhost;dbname=cardsapp','root','root');"

### How do I get set up? ###

* clone the project
* install node.js on the system
* cd to project directory
* inside castikocardsproject/src/app/app.config.js make sure the link to pointing correctly to API project on line no. 15
    "cardsApp.constant('serviceBase', 'http://localhost:8888/cardsappapi/');"
* make sure server has APC enabled for caching

* run commands:
* 	1. npm install
* 	2. webpack --watch & npm start &


