"use strict";

var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.controller('profileCntrl', ['$scope', 'authService', '$q','Upload', 'serviceBase', 'dashboardService','notify', function ($scope, authService, $q, Upload, serviceBase, dashboardService, notify) {

    var profile = this;

    profile.ageEditable = false;
    profile.nameEditable = false;
    profile.skillsEditable = false;
    profile.filesArr = [];
    profile.skills = [];

    profile.updateAge = updateAge;
    profile.updateName = updateName;
    profile.updateSkills = updateSkills;
    profile.editProfile = editProfile;
    profile.addSkill = addSkill;
    profile.removeSkill =removeSkill;
    profile.imageserviceBase = serviceBase+"images/";
    getProfile();
    getPics();

    profile.addFile = function (file) {
        if(file){
            Upload.imageDimensions(file).then(function(dimensions){
                console.log(dimensions.width, dimensions.height);
                if(dimensions.width<1280){
                    notify("Image dimensions should be atleast 1280 x 960");
                }else if(dimensions.height<960){
                    notify("Image dimensions should be atleast 1280 x 960");
                }else{
                    var options = {width:1280,height:960};
                    Upload.resize(file, options).then(function(resizedFile){
                        var user =  authService.getCurrentUser();
                        Upload.upload({
                            url: serviceBase + 'uploadPic',
                            data: {file: file, 'user_id': user.id}
                        }).then(function (res) {
                            if(res.data.message=="Successfully saved picture" && !res.data.pic_message){
                                notify(res.data.message);
                                getPics();
                                // profile.filesArr.push(res.data.pic_name);
                            }else if(res.data.message=="Successfully saved picture" && res.data.pic_message){
                                notify(res.data.pic_message);
                                getPics();
                                // profile.filesArr.push(res.data.pic_name));
                            }
                        }, function (resp) {
                            console.log('Error status: ' + resp.status);
                        }, function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                        });
                    });
                }
            });
        }
    };

    profile.removePics = function (pic_name) {
        var user =  authService.getCurrentUser();
        dashboardService.deletePic(user.id, pic_name, function (res) {
            if(res.message=="Updated picture details"){
                getPics();
            }
        });
    }

    function getPics() {
        var user =  authService.getCurrentUser();
        dashboardService.getPics(user.id, function (res) {
            if (res) {
                if (res.pics){
                    profile.filesArr = res.pics;
                }else{
                    profile.filesArr = [];
                }
            }
        });
    }

    function makeProfilePicture(pic_name){
        dashboardService.setProfilePicture(user.id, function (res) {
            if (res) {
            }
        });
    }

    function updateAge(){
        profile.ageEditable = !profile.ageEditable;
        updateProfile();
    }

    function updateName(){
        profile.nameEditable = !profile.nameEditable;
        updateProfile();
    }

    function updateSkills(){
        profile.skillsEditable = !profile.skillsEditable;
        updateProfile();
    }

    function updateProfile(){
        var user =  authService.getCurrentUser();
        dashboardService.saveProfile(user.id,profile.userDetails, function (res) {
            notify(res.message);
            getProfile();
        });
    }

    function editProfile(){
        profile.skillsEditable = !profile.skillsEditable;;
        profile.ageEditable = !profile.ageEditable;;
        profile.nameEditable = !profile.nameEditable;;
    }

    function getProfile(){
        var currentUser =  authService.getCurrentUser();
        dashboardService.getProfile(currentUser.id, function (res) {
            if(res && res.userDetails){
                profile.userDetails = res.userDetails;
                if(profile.userDetails.skills!='' && profile.userDetails.skills !=null){
                    profile.skills= profile.userDetails.skills.split(',');
                }
            }
        });
    }

    function addSkill(newSkill){
        profile.skillsEditable = !profile.skillsEditable;
        if (profile.skills.indexOf(newSkill) == -1 && newSkill!='' && newSkill!=null) {
            profile.skills.push(newSkill);
        }
        profile.userDetails.skills = profile.skills.join(",");
        updateProfile();
    }

    function removeSkill(newSkill){
        profile.skills.splice(newSkill,1);
        profile.userDetails.skills = profile.skills.join(",");
        updateProfile();
    }

}]);