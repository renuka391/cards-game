"use strict";
var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.service('dashboardService', ['localStorageService', '$http', 'serviceBase', function (localStorageService, $http, serviceBase) {
  this.saveGame = function (user_id, lists, callback) {
    $http.post(serviceBase + 'saveGame', {
        'lists': lists,
        'user_id': user_id
      })
      .success(function (response) {
        callback(response);
      })
      .error(function (response) {
        callback(response);
      });
  }
  this.getGame = function (user_id, callback) {
    $http.get(serviceBase + 'getGame', {
        params:{'user_id': user_id}
      })
      .success(function (response) {
        callback(response);
      })
      .error(function (response) {
        callback(response);
      });
  }
  this.getPics = function (user_id, callback) {
    $http.get(serviceBase + 'getPics', {
        params:{'user_id': user_id}
      })
      .success(function (response) {
        callback(response);
      })
      .error(function (response) {
        callback(response);
      });
  }
  this.saveProfile = function (user_id, userDetails, callback) {
    $http.post(serviceBase + 'updateProfile', {
        'userDetails': userDetails,
        'user_id': user_id
      })
      .success(function (response) {
        callback(response);
      })
      .error(function (response) {
        callback(response);
      });
  }
  this.getProfile = function (user_id, callback) {
    $http.get(serviceBase + 'getProfile', {
        params:{'user_id': user_id}
      })
      .success(function (response) {
        callback(response);
      })
      .error(function (response) {
        callback(response);
      });
  }
  this.deletePic = function (user_id, pic_name, callback) {
    $http.post(serviceBase + 'deletePic', {
        'pic_name': pic_name,
        'user_id': user_id
      })
      .success(function (response) {
        callback(response);
      })
      .error(function (response) {
        callback(response);
      });
  }
}]);
