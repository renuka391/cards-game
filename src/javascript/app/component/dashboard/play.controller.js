"use strict";

var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.controller('playCntrl', ['$scope', 'authService', '$q', 'serviceBase', 'dashboardService','notify', function ($scope, authService, $q, serviceBase, dashboardService, notify) {

    var lists = [
        {
            label: "Spades",
            allowedTypes: ['spade'],
            max: 13,
            card: [
            ]
        },
        {
            label: "Heart",
            allowedTypes: ['heart'],
            max: 13,
            card: [
            ]
        },
        {
            label: "Club",
            allowedTypes: ['club'],
            max: 13,
            card: [
            ]
        },
        {
            label: "Diamond",
            allowedTypes: ['diamond'],
            max: 13,
            card: [
            ]
        },
        {
            label: "Cards",
            allowedTypes: ['spade', 'heart','club','diamond'],
            max: 52,
            card: [
                {name: "2 of spade", type: "spade"},
                {name: "3 of spade", type: "spade"},
                {name: "4 of spade", type: "spade"},
                {name: "5 of spade", type: "spade"},
                {name: "6 of spade", type: "spade"},
                {name: "7 of spade", type: "spade"},
                {name: "8 of spade", type: "spade"},
                {name: "9 of spade", type: "spade"},
                {name: "10 of spade", type: "spade"},
                {name: "J of spade", type: "spade"},
                {name: "Q of spade", type: "spade"},
                {name: "K of spade", type: "spade"},
                {name: "A of spade", type: "spade"},
                {name: "2 of heart", type: "heart"},
                {name: "3 of heart", type: "heart"},
                {name: "4 of heart", type: "heart"},
                {name: "5 of heart", type: "heart"},
                {name: "6 of heart", type: "heart"},
                {name: "7 of heart", type: "heart"},
                {name: "8 of heart", type: "heart"},
                {name: "9 of heart", type: "heart"},
                {name: "10 of heart", type: "heart"},
                {name: "J of heart", type: "heart"},
                {name: "Q of heart", type: "heart"},
                {name: "K of heart", type: "heart"},
                {name: "A of heart", type: "heart"},
                {name: "2 of club", type: "club"},
                {name: "3 of club", type: "club"},
                {name: "4 of club", type: "club"},
                {name: "5 of club", type: "club"},
                {name: "6 of club", type: "club"},
                {name: "7 of club", type: "club"},
                {name: "8 of club", type: "club"},
                {name: "9 of club", type: "club"},
                {name: "10 of club", type: "club"},
                {name: "J of club", type: "club"},
                {name: "Q of club", type: "club"},
                {name: "K of club", type: "club"},
                {name: "A of club", type: "club"},
                {name: "2 of diamond", type: "diamond"},
                {name: "3 of diamond", type: "diamond"},
                {name: "4 of diamond", type: "diamond"},
                {name: "5 of diamond", type: "diamond"},
                {name: "6 of diamond", type: "diamond"},
                {name: "7 of diamond", type: "diamond"},
                {name: "8 of diamond", type: "diamond"},
                {name: "9 of diamond", type: "diamond"},
                {name: "10 of diamond", type: "diamond"},
                {name: "J of diamond", type: "diamond"},
                {name: "Q of diamond", type: "diamond"},
                {name: "K of diamond", type: "diamond"},
                {name: "A of diamond", type: "diamond"}

            ]
        }
    ];

    getGame();

    // Model to JSON for demo purpose
    $scope.$watch('lists', function(lists) {
        $scope.modelAsJson = angular.toJson(lists, true);
    }, true);

    $scope.shuffle = function(o){ 
        for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    };

    function getGame(){
        var user =  authService.getCurrentUser();
        dashboardService.getGame(user.id, function (res) {
            if(res.user_game){
                $scope.lists = JSON.parse(res.user_game.lists);
            }else{
                $scope.lists = lists;
            }
        });
    };

    $scope.save = function(){
        var user =  authService.getCurrentUser();
        dashboardService.saveGame(user.id,angular.toJson($scope.lists), function (res) {
            notify(res.message);
        });
    }

    $scope.restart = function(){
        $scope.lists = lists;
        console.log($scope.lists);
    }

}]);