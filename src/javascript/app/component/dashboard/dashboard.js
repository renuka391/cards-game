"use strict";

var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/login');

  $stateProvider    
    .state('app', {
      url: '/app',
      templateUrl: '/ng-tpl/dashboard',
      abstract: true,
      resolve: {
        checkAuthentication: ["$q", "$state", "$timeout", "authService", function ($q, $state, $timeout, authService) {
          var deferred = $q.defer();
          // $timeout is an example; it also can be an xhr request or any other async function
          $timeout(function () {              
            if (!authService.getToken()) {
              // user is not logged, do not proceed
              // instead, go to a different page
              $state.go('login');
              deferred.reject();
            } else {
              // everything is fine, proceed
              deferred.resolve();
            }
          });

          return deferred.promise;
        }]
      }
    })    
    .state('app.dashboard', {
      url: '/dashboard'
    }) 
    .state('profile', {
      url: '/profile',
      templateUrl: '/ng-tpl/profile',
      resolve: {
        checkAuthentication: ["$q", "$state", "$timeout", "authService", function ($q, $state, $timeout, authService) {
          var deferred = $q.defer();
          // $timeout is an example; it also can be an xhr request or any other async function
          $timeout(function () {              
            if (!authService.getToken()) {
              // user is not logged, do not proceed
              // instead, go to a different page
              $state.go('login');
              deferred.reject();
            } else {
              // everything is fine, proceed
              deferred.resolve();
            }
          });

          return deferred.promise;
        }]
      }
    })  
    .state('play', {
      url: '/play',
      templateUrl: '/ng-tpl/play',
      resolve: {
        checkAuthentication: ["$q", "$state", "$timeout", "authService", function ($q, $state, $timeout, authService) {
          var deferred = $q.defer();
          // $timeout is an example; it also can be an xhr request or any other async function
          $timeout(function () {              
            if (!authService.getToken()) {
              // user is not logged, do not proceed
              // instead, go to a different page
              $state.go('login');
              deferred.reject();
            } else {
              // everything is fine, proceed
              deferred.resolve();
            }
          });

          return deferred.promise;
        }]
      }
    });          
}]);
