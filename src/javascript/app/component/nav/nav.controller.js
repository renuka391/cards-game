var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.controller('navCtrl', ['$scope', '$location', '$window', 'authService', 'localStorageService', function ($scope, $location, $window, authService, localStorageService) {
  var nav = this;
  nav.profile = authService.getCurrentUser();
  nav.isAuth = authService.isAuth;
}]);

module.exports = cardsApp;
