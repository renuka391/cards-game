"use strict";
var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.service('loginService', ['localStorageService', '$http', 'serviceBase', function (localStorageService, $http, serviceBase) {
  this.login = function (email, password, callback) {

    $http.post(serviceBase + '/login', {
      'email': email,
      'password': password
    })
      .success(function (response) {
        callback(response);
      })
      .error(function (response) {
        callback(response);
      });
  }
}]);
