"use strict";
var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.service('signupService', ['localStorageService', '$http', 'serviceBase', function (localStorageService, $http, serviceBase) {
  this.do = function (name, email, password, callback) {
    $http.post(serviceBase + '/register', {
        'name': name,
        'email': email,        
        'password': password
      })
      .success(function (response) {
        callback(response);
      })
      .error(function (response) {
        callback(response);
      });
  }
}]);
