var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.controller("signupCtrl", ['$http', '$q', '$scope', '$window', 'authService', 'signupService', 'notify', '$auth', '$state', function ($http, $q, $scope, $window, authService, signupService, notify, $auth, $state) {
	var signupCtrl = this;
    signupCtrl.do = function () {
        signupService.do(signupCtrl.name, signupCtrl.email, signupCtrl.password, function (res) {
            notify(res.message);
            if(res.message=="User successfully created"){
            	$state.go('login');
            }else{
	            signupCtrl.email = "";
	            signupCtrl.name = "";
	            signupCtrl.password = "";
	        }
        });
    }
  
}]);

module.exports = cardsApp;
