"use strict";
var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.service('authService', ['localStorageService', function (localStorageService) {
  var service = this,
    currentUser = null,
    token = null;

  service.setCurrentUser = function (user) {
    currentUser = user;
    localStorageService.set('cardsUser', user);
    return currentUser;
  };

  service.getCurrentUser = function () {
    if (!currentUser) {
      currentUser = localStorageService.get('cardsUser');
    }
    return currentUser;
  };

  service.getToken = function () {
    if (!token) {
      token = localStorageService.get('cardsToken');
    }
    return token;
  }

  service.setToken = function (token) {
    token = token;
    localStorageService.set('cardsToken', token);
    return token;
  }

  service.logout = function (callback) {
    localStorageService.clearAll();
    currentUser = null;
    token = null;
    callback();
  };

  service.isAuth = function () {
    return (service.getToken() != null || service.getToken() != undefined);
  };


}]);
