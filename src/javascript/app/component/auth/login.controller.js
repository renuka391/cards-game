var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.controller("loginCtrl", ['$http', '$q', '$scope', '$window', 'authService', 'notify', '$auth', 'loginService', '$state', function ($http, $q, $scope, $window, authService, notify, $auth, loginService, $state) {

  $scope.login = function () {
    loginService.login($scope.loginInfo.email,
      $scope.loginInfo.password,
      function(response) {
        if (response.token) {
          authService.setToken(response.token);
          authService.setCurrentUser(response.user);
          $state.go('app.dashboard');
        } else {
          notify(response.message);
        }
      }
    );
  }

}]);

module.exports = cardsApp;
