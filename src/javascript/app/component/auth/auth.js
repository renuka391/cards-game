"use strict";

var cardsApp = require('../../app.module.js').cardsApp;

cardsApp.config(['$stateProvider', function ($stateProvider) {
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: '/ng-tpl/login-page',      
      resolve: {
        checkAuthentication: ["$q", "$state", "$timeout", "authService", function ($q, $state, $timeout, authService) {
          var deferred = $q.defer();
          // $timeout is an example; it also can be an xhr request or any other async function
          $timeout(function () {
            if (authService.getToken()) {
              // user is not logged, do not proceed
              // instead, go to a different page              
              $state.go('app.dashboard');
              deferred.reject();
            } else {
              // everything is fine, proceed
              deferred.resolve();
            }
          });
          return deferred.promise;
        }]
      }
    })
    .state('signup', {
      url: '/signup',
      templateUrl: '/ng-tpl/signup-page',      
      resolve: {
        checkAuthentication: ["$q", "$state", "$timeout", "authService", function ($q, $state, $timeout, authService) {
          var deferred = $q.defer();
          // $timeout is an example; it also can be an xhr request or any other async function
          $timeout(function () {
            if (authService.getToken()) {
              // user is not logged, do not proceed
              // instead, go to a different page              
              $state.go('app.dashboard');
              deferred.reject();
            } else {
              // everything is fine, proceed
              deferred.resolve();
            }
          });
          return deferred.promise;
        }]
      }
    })
    .state('logout', {
      url: '/logout',
      resolve: {
        clearSession: ['$state', '$q', '$timeout', 'localStorageService', 'authService', '$auth', function ($state, $q, $timeout, localStorageService, authService, $auth) {          
          var deferred = $q.defer();
          $auth.logout();
          $timeout(function () {
            authService.logout(function () {
                $state.go('login');
              }
            );
            deferred.resolve();

          });
          return deferred.promise;
        }]
      }
    })
}]);
