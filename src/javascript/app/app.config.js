var cardsApp = require('./app.module.js').cardsApp;
var authService = require('./component/auth/auth.service.js');

cardsApp.config(["$mdThemingProvider", function ($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('light-blue')
    .accentPalette('grey');
}]);

cardsApp.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = false;
}]);


cardsApp.constant('serviceBase', 'http://localhost:8888/cardsappapi/');

cardsApp.factory('sessionInjector', ['authService', function (authService) {
  var sessionInjector = {
    request: function (config) {
      if (authService.isAuth()) {
        config.headers.Authorization ='Bearer ' +  authService.getToken();
      }
      return config;
    }
  }
  return sessionInjector;
}]);


cardsApp.service('authInterceptor', ['$q', '$window', function ($q, $window) {
  var service = this;

  service.responseError = function (response) {
    if (response.status == 401) {
      //$window.location.href = "/app#/login";
    }
    return $q.reject(response);
  };
}]);

cardsApp.factory('timeoutHttpIntercept', ['$rootScope', '$q', function ($rootScope, $q) {
    return {
      'request': function(config) {
        config.timeout = 30000;
        return config;
      }
    };
 }]);

cardsApp.config(['$httpProvider', function ($httpProvider) {
  $httpProvider.interceptors.push('sessionInjector');
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.interceptors.push('timeoutHttpIntercept');
}]);

module.exports = cardsApp;
