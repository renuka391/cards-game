
var userAppDependencies = ['Cards'];
var appDependencies = [
'ngMaterial', 
'ngAnimate', 
'ngResource', 
'ngSanitize', 
'LocalStorageModule',
'md.data.table', 
'satellizer',
'cgNotify', 
'angular-loading-bar',
'ui.router',
'nvd3',
'dndLists',
'ngFileUpload'
];

module.exports = {
    cardsApp: angular.module('Cards', appDependencies),
}
