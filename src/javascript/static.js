require('angular-material/angular-material.min.css');
require('@cgross/angular-notify/dist/angular-notify.min.css');

require('angular-material-data-table/dist/md-data-table.min.css');
require('../../public/stylesheets/nv.d3.min.css');
require('../../public/stylesheets/animate.min.css');
require('../../public/stylesheets/homepage.css');

require('angular/angular.min.js');
require('angular-material/angular-material.min.js');
require('angular-animate/angular-animate.min.js');
require('angular-sanitize/angular-sanitize.min.js');
require('angular-resource/angular-resource.min.js');
require('angular-aria/angular-aria.min.js');
require('angular-ui-router/release/angular-ui-router.min.js');
require('angular-local-storage/dist/angular-local-storage.js');
require('angular-material-data-table/dist/md-data-table.min.js');
require('angular-nvd3/dist/angular-nvd3.min.js');
require('@cgross/angular-notify/dist/angular-notify.min.js');
require('satellizer/dist/satellizer.min.js');
require('angular-loading-bar/build/loading-bar.min.js');
require('angular-drag-and-drop-lists/angular-drag-and-drop-lists.js');
require('ng-file-upload/dist/ng-file-upload-shim.min.js');
require('ng-file-upload/dist/ng-file-upload.min.js');