require('./app/app.module.js');
require('./app/app.config.js');

// auth
require('./app/component/auth/auth.js');
require('./app/component/auth/auth.service.js');
require('./app/component/auth/login.controller.js');
require('./app/component/auth/login.service.js');
require('./app/component/auth/signup.controller.js');
require('./app/component/auth/signup.service.js');

//dashboard
require('./app/component/dashboard/dashboard.js');
require('./app/component/dashboard/dashboard.controller.js');
require('./app/component/dashboard/dashboard.service.js');
require('./app/component/dashboard/play.controller.js');
require('./app/component/dashboard/profile.controller.js');

// standalone controllers
require('./app/component/main/main.controller.js');
require('./app/component/nav/nav.controller.js');