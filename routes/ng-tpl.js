var express = require('express');
var router = express.Router();

router.get('/:route', function(req, res, next) {

  res.render('./ng-tpl/'+ req.params.route +'.ejs', { }, function(err, html) {
    if(err) {
        res.status(404); // File doesn't exist        
        next();
    } else {
        res.send(html);   
    }
  });  
});

router.get('*', function(req, res){
  res.status(404)        // HTTP status 404: NotFound
   .send('Not found');
});

module.exports = router;
