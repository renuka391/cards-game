var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function (req, res, next) { 
  res.render('index', { title: 'Cards app.', meta: '', keywords: '', metaImage: '' });
});

router.get('/app*', function (req, res, next) {
  res.render('./ng-tpl/app-layout-tpl', { title: 'Dashboard', style: 'tools', meta: '', keywords: '', metaImage: '' });
});



module.exports = router;
